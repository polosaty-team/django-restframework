from django.urls import path
from .admin import admin
from . import views


from django.conf.urls import url, include
from .models import Question
from .models import Choice

from rest_framework import routers, serializers, viewsets


# Serializers define the API representation.
# TODO: move serializers to serializers.py
class ChoiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Choice
        fields = ('id', 'question', 'choice_text', 'votes')


class QuestionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Question
        fields = ('id', 'question_text', 'pub_date', 'choices')


# ViewSets define the view behavior.
# TODO: move views to views.py or api.py or v1/api.py
class QuestionViewSet(viewsets.ModelViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class ChoiseViewSet(viewsets.ModelViewSet):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'questions', QuestionViewSet)
router.register(r'choices', ChoiseViewSet)



urlpatterns = [
    path('', views.index, name='index'),
    # path('admin/', admin.site.urls, name='polls_admin'),
    url(r'^rest/', include(router.urls)),
]
