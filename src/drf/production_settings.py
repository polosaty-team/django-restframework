from .settings import *

STATICFILES_DIRS = [
    os.environ.get('DIR_ASSETS', os.path.join(BASE_DIR, "assets")),
]

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': os.path.join('/', 'frontend/webpack-stats.prod.json'),
    }
}
