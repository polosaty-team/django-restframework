const initialState = [
    // { text: "Write code!" }
];


export default function polls(state = initialState, action) {
    switch (action.type) {
        case 'FETCH_POLLS':
            // return [...state, ...action.polls];
            return [...action.polls];
        case 'ADD_POLL':
            // return [...state, ...action.polls];
            return [action.session, ...state];
        default:
            return state;
    }
}
