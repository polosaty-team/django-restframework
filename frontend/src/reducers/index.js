import { combineReducers } from 'redux';
import polls from "./polls";


const someApp = combineReducers({
    polls,
})

export default someApp;
