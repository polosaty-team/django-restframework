import React, { Component } from 'react';
import { Route, Switch, BrowserRouter, Link } from 'react-router-dom';

// import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import 'react-notifications/lib/notifications.css';


import { Provider } from "react-redux";
import someApp from "./reducers";

import Polls from "./components/Polls";

import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import { NotificationContainer } from 'react-notifications';

let store = createStore(someApp, applyMiddleware(thunk));


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
          </ul>

          <hr />
          <Switch>
            <Route exact path="/" component={Polls} />
            {/* <Route path="/some-by-poll/:id(.*)/" component={SomeByPoll} /> */}

          </Switch>
        </BrowserRouter>

        <NotificationContainer />

      </Provider>

    );
  }
}

// function SomeByPoll({ match }) {
//   return (
//     <Some poll_id={ match.params.id } />
//   );
// }
export default App;
