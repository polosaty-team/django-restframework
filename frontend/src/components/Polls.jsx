import React, { Component } from 'react';
import { connect } from 'react-redux';
import { polls } from "../actions";
// import { Link } from 'react-router-dom';
// import * as Url from "url-parse";

// import ReactModal from "react-modal";
// import { NotificationManager } from 'react-notifications';


class Choice extends Component {
    render(){
        return this.props.url
    }
}

class Polls extends Component {

    constructor(){
        super();
        this.state = {
            text: "",
            showModal: false,

        }

        this.submitPoll = this.submitPoll.bind(this);
        this.resetForm = this.resetForm.bind(this);
    }

    componentDidMount() {
        this.props.fetchPolls();
    }

    resetForm(e) {
        // e.preventDefault();
        this.setState({ text: "" });
    }

    submitPoll(e) {
        e.preventDefault();
        this.props.addPoll(this.state.text).then(this.resetForm)

    }

    render() {
        let me = this;
        const refreshButton = (
            <button
                className="button-link"
                style={{ fontSize: "large" }}
                onClick={() => this.props.fetchPolls()}>
                <span aria-label="refresh" role="img">🔄</span>
            </button>
        );

        return (
            <div>

                <h2>
                    {refreshButton}
                    polls
                </h2>
                {/* <form onSubmit={ this.submitPoll }>
                    <label htmlFor="url-input">text</label>
                    <input
                        id="text-input"
                        type="text"
                        name="text"
                        value={this.state.text}
                        onChange={(e) => this.setState({ text: e.target.value })}
                        required/>

                    <input type="submit" value="submit" />
                </form> */}
                <hr />

                <table style={{width: "100hv"}} className="polls">
                    <tbody>
                        {this.props.polls.map(poll => (
                            <tr key={poll.id}>
                                <td>{poll.question_text}</td>
                                <td>{poll.pub_date}</td>
                                <td>
                                    <table>
                                    {poll.choices.map(choice => (
                                        <tr>
                                            <td>
                                                <Choice url={choice} />
                                            </td>
                                        </tr>
                                    ))}
                                    </table>
                                    {/* <Link to={`${poll.id}`}>
                                        questions
                                    </Link> */}
                                </td>

                            {/* TODO: pagination */}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        polls: state.polls,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchPolls: () => {
            dispatch(polls.fetchPolls());
        },
        addPoll: (text) => {
            return dispatch(polls.addPoll(text));
        },
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Polls);
