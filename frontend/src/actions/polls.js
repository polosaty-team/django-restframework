export const fetchPolls = () => {
    return dispatch => {
        let headers = { "Content-Type": "application/json" };
        return fetch("/polls/rest/questions/", { headers, })
            .then(res => res.json())
            .then(polls => {
                return dispatch({
                    type: 'FETCH_POLLS',
                    polls
                })
            })
    }
}

export const addPoll = url => {
    return dispatch => {
        let headers = {
            "Content-Type": "application/json"
            // 'X-CsrfToken'
        };
        let body = JSON.stringify({ url: url, });
        return fetch("/polls/rest/questions/", { headers, method: "POST", body })
            .then(res => res.json())
            .then(session => {
                return dispatch({
                    type: 'ADD_POLL',
                    session
                })
            })
    }
}
