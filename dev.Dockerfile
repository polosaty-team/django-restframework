FROM python:3.6
LABEL maintainer='m.aksarin@gmail.com'

COPY requirements.pip /requirements.pip

RUN pip3 install -r /requirements.pip

WORKDIR /app

RUN adduser \
    --disabled-password \
    --no-create-home \
    --shell /bin/bash \
    --gecos "" \
    --uid 1000 \
    --home /app \
    app

# RUN chown -R app:app ${DIR_APP}

# RUN pip3 install wdb ipython pudb flower
RUN pip3 install wdb ipython pudb

# Устанавливаем пользвоаетля от имени которого будет работать приложение
USER app
